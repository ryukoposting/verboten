# frozen_string_literal: true

# Copyright 2018 Richard Davis
#
# This file is part of verboten.
#
# verboten is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# verboten is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with verboten.  If not, see <http://www.gnu.org/licenses/>.

require 'json'

module Verboten
  ##
  # = Seen
  # Author::    Richard Davis
  # Copyright:: Copyright 2019 Mushaka Solutions
  # License::   GNU Public License 3
  #
  # Subroutine to log and return information regarding the last time a user was seen on the channel.
  module Seen
    SEEN_FILE = ENV['HOME'] + '/.verboten/seen.json'

    def create_seen_file
      f = File.new(SEEN_FILE, 'w+')
      f.puts('[]')
      f.close
    end

    def read_from_seen_file
      create_seen_file unless File.exist?(SEEN_FILE)
      f = File.read(SEEN_FILE)
      JSON.parse(f)
    end

    def log_to_seen_file(nick, data)
      contents = read_from_seen_file
      found = false

      unless contents.length.zero?
        contents.each do |user|
          if !(user['nick'].nil?) && user['nick'].casecmp(nick).zero?
            user['message'] = data.slice(3..-1).join(' ').force_encoding('UTF-8')[1..-1]
            user['date'] = DateTime.now.new_offset('+0.00').strftime('%v, %r UTC')
            found = true
          end
        end
      end

      if !found
        contents.push({
          nick: nick,
          message: data.slice(3..-1).join(' ').force_encoding('UTF-8')[1..-1],
          date: DateTime.now.new_offset('+0.00').strftime('%v, %r UTC')
        })
      end

      unless contents.length.zero?
        f = File.open(SEEN_FILE, 'w+')
        f.write(JSON.pretty_generate(contents))
        f.close
      end
    end

    def last_seen_user(nick)
      return 'No search term given. 😕' if nick.nil?

      contents = read_from_seen_file
      response = { message: '', date: '' }
      found = false

      unless contents.length.zero?
        contents.each do |user|
          if !(user['nick'].nil?) && user['nick'].casecmp(nick).zero?
            response[:message] = user['message']
            response[:date] = user['date']
            found = true
          end
        end
      end

      return "#{nick} was last seen #{response[:date]} saying: \"#{response[:message]}\"" if found
      'I have not seen this user yet. 😕'
    end
  end
end
